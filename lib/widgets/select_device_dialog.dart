import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:provider/provider.dart';

import '../providers/device_provider.dart';

class SelectDeviceDialog extends StatelessWidget{
    final List<BluetoothDevice> devices;

    SelectDeviceDialog({
        required this.devices
    });

    @override
    Widget build(BuildContext context){
        if(this.devices.isEmpty){
            return AlertDialog(
                title: Text('No se encontraron dispositivos'),
                actions: [
                    FlatButton(
                        child: Text('Ok'),
                        onPressed: (){
                            Navigator.of(context).pop();
                        }
                    )
                ]
            );
        }

        return AlertDialog(
            title: Text('Selecciona un dispositivo'),
            content: Container(
                width: double.maxFinite,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: devices.length,
                    itemBuilder: (context, index){
                        return Column(children: [
                            ListTile(
                                leading: Icon(Icons.bluetooth),
                                title: Text(devices[index].name),
                                onTap: () async{
                                    try{
                                        Provider.of<DeviceProvider>(
                                            context,
                                            listen: false
                                        ).setDevice(devices[index]);
                                        Navigator.of(context).pop();
                                    }catch(error){
                                        await showDialog(
                                            context: context,
                                            builder: (context){
                                                return AlertDialog(
                                                    title: Text('Error'),
                                                    content: Text(error.toString()),
                                                    actions: [
                                                        FlatButton(
                                                            child: Text('Ok'),
                                                            onPressed: (){
                                                                Navigator.of(context).pop();
                                                            }
                                                        )
                                                    ]
                                                );
                                            }
                                        );
                                    }
                                }
                            ),
                            Divider()
                        ]);
                    }
                )
            ),
            actions: [
                FlatButton(
                    child: Text('Cancelar'),
                    onPressed: (){
                        Navigator.of(context).pop();
                    },
                )
            ]
        );
    }
}
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import './select_device_dialog.dart';

class AppDrawer extends StatelessWidget{
    @override
    Widget build(BuildContext context){
        return Drawer(child: Column(children: [
            AppBar(
                title: Text('Selecciona una opción'),
                automaticallyImplyLeading: false
            ),
            ListTile(
                leading: Icon(Icons.bluetooth),
                title: Text('Seleccionar dispositivo'),
                onTap: () async{
                    await showDialog(
                        context: context,
                        builder: (context){
                            return FutureBuilder(
                                future: Future.wait([FlutterBlue.instance.scan(timeout: const Duration(seconds: 4)).toList(), FlutterBlue.instance.connectedDevices]),
                                builder: (context, snapshot){
                                    if(snapshot.connectionState == ConnectionState.waiting){
                                        return AlertDialog(
                                            title: const Text('Buscando dispositivos'),
                                            content: const Center(
                                                heightFactor: 3,
                                                child: CircularProgressIndicator()
                                            ),
                                            actions: [
                                                TextButton(
                                                    child: const Text('Cancelar'),
                                                    onPressed: () async {
                                                        await FlutterBlue.instance.stopScan();
                                                        Navigator.of(context).pop();
                                                    }
                                                )
                                            ]
                                        );
                                    }

                                    // devicesOperation.cancel();
                                    final scannedDevices = ((snapshot.data! as List)[0] as List<ScanResult>).map((scan){
                                        return scan.device;
                                    }).where((device){
                                        return device.name.isNotEmpty;
                                    }).toList();
                                    final connectedDevices = (snapshot.data! as List)[1] as List<BluetoothDevice>;

                                    return SelectDeviceDialog(devices: scannedDevices + connectedDevices);
                                }
                            );
                        }
                    );
                }
            )
        ]));
    }
}
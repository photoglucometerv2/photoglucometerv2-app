import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import '../models/point.dart';

class Chart extends StatelessWidget{
    final List<Point> ppg;

    Chart({
        required this.ppg
    });

    Widget build(BuildContext context){
        return charts.TimeSeriesChart(
            [charts.Series<Point, DateTime>(
                id: 'PPG',
                colorFn: (_, __){
                    return charts.MaterialPalette.green.shadeDefault;
                },
                domainFn: (Point point, _){
                    return point.time;
                },
                measureFn: (Point point, _){
                    return point.value;
                },
                data: ppg.sublist(ppg.length >= 300 ? ppg.length - 300 : 0)
            )],
            animate: false,
            primaryMeasureAxis: const charts.NumericAxisSpec(
                tickProviderSpec: charts.BasicNumericTickProviderSpec(zeroBound: false),
                renderSpec: charts.NoneRenderSpec(),
            ),
            domainAxis: const charts.DateTimeAxisSpec(renderSpec: charts.NoneRenderSpec())
        );
    }
}
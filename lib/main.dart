import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gluco_mobile_app/providers/device_provider.dart';
import 'package:gluco_mobile_app/screens/collector_screen.dart';
import 'package:gluco_mobile_app/screens/create_ppg_screen.dart';
import 'package:provider/provider.dart';

import './providers/ppg_provider.dart';

void main() {
  runApp(const GlucoApp());
}

class GlucoApp extends StatelessWidget {
  const GlucoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: PpgProvider()),
          ChangeNotifierProvider.value(value: DeviceProvider())
        ],
        child: MaterialApp(
          title: 'Gluco Sampler',
          theme: ThemeData(
              primarySwatch: Colors.blue,
              colorScheme:
                  ColorScheme.fromSwatch().copyWith(secondary: Colors.amber),
              textTheme: ThemeData.light().textTheme.copyWith(
                    button: const TextStyle(
                      color: Colors.white,
                    ),
                  )),
          home: Consumer<DeviceProvider>(builder: (context, deviceProvider, _) {
            return CollectorScreen(device: deviceProvider.device);
          }),
          routes: {
            CreatePpgScreen.routeName: ((context) => CreatePpgScreen()),
            CollectorScreen.routeName: ((context) =>
                Consumer<DeviceProvider>(builder: (context, deviceProvider, _) {
                  return CollectorScreen(device: deviceProvider.device);
                }))
          },
        ));
  }
}

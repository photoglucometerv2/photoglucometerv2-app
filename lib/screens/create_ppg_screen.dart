import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gluco_mobile_app/screens/collector_screen.dart';
import 'package:gluco_mobile_app/widgets/app_drawer.dart';
import 'package:provider/provider.dart';

import '../models/ppg.dart';
import '../providers/ppg_provider.dart';

class CreatePpgScreen extends StatefulWidget {
  static const routeName = '/create-ppg';

  @override
  _CreatePpgScreenState createState() {
    return _CreatePpgScreenState();
  }
}

class _CreatePpgScreenState extends State<CreatePpgScreen> {
  bool _processing = false;
  final _createPpgForm = GlobalKey<FormState>();
  final _bpDiastolicFocusNode = FocusNode();
  final _trueGlucoFocusNode = FocusNode();
  final _subjectHeightFocusNode = FocusNode();
  final _subjectAgeFocusNode = FocusNode();
  final _subjectWeightFocusNode = FocusNode();
  final _subjectSexFocusNode = FocusNode();
  final _subjectIsDiabeticFocusNode = FocusNode();
  final _subjectEmailFocusNode = FocusNode();
  final _ppgStoreRequest = PpgStoreRequest(
      ppg: '',
      ppgAlt: '',
      ecg: '',
      bpSystolic: 0,
      bpDiastolic: 0,
      trueGluco: 0.0,
      subjectAge: 0,
      subjectHeight: 0.0,
      subjectWeight: 0.0,
      subjectSex: 'male',
      subjectIsDiabetic: false,
      subjectEmail: '');

  @override
  void dispose() {
    _subjectAgeFocusNode.dispose();
    _subjectHeightFocusNode.dispose();
    _subjectWeightFocusNode.dispose();
    _subjectSexFocusNode.dispose();
    _subjectIsDiabeticFocusNode.dispose();
    _subjectEmailFocusNode.dispose();
    super.dispose();
  }

  Future<void> storePpg() async {
    setState(() {
      _processing = true;
    });

    if (!_createPpgForm.currentState!.validate()) {
      return;
    }
    _createPpgForm.currentState!.save();

    try {
      await Provider.of<PpgProvider>(context, listen: false)
          .store(_ppgStoreRequest);
      await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
                title: const Text('Solicitud enviada exitosamente'),
                content: const Text('Su información ha sido recibida.'),
                actions: [
                  TextButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                  )
                ]);
          });
    } catch (error) {
      await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
                title: const Text('Error'),
                content: Text(error.toString()),
                actions: [
                  TextButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ]);
          });
    }

    setState(() {
      _processing = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    _ppgStoreRequest.ppg = args['ppg'];
    _ppgStoreRequest.ppgAlt = args['ppgAlt'];
    _ppgStoreRequest.ecg = args['ecg'];

    const doubleInitialValue = '';
    const dropdownInitialValue = null;

    return Scaffold(
        appBar: AppBar(title: const Text('Recolectar datos')),
        drawer: AppDrawer(),
        body: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _createPpgForm,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              const Text(
                  'A continuación, ingrese sus datos para poder enviar con su curva PPG:'),
              TextFormField(
                  initialValue: doubleInitialValue,
                  decoration:
                      const InputDecoration(labelText: 'Presión sistólica'),
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_bpDiastolicFocusNode);
                  },
                  validator: (bpSystolic) {
                    if (bpSystolic!.isEmpty) {
                      return 'La presión sistólica es requerida.';
                    }

                    return null;
                  },
                  onSaved: (bpSystolic) {
                    _ppgStoreRequest.bpSystolic = int.parse(bpSystolic!);
                  }),
              TextFormField(
                  initialValue: doubleInitialValue,
                  decoration:
                      const InputDecoration(labelText: 'Presión diastólica'),
                  textInputAction: TextInputAction.next,
                  focusNode: _bpDiastolicFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_trueGlucoFocusNode);
                  },
                  validator: (bpDiastolic) {
                    if (bpDiastolic!.isEmpty) {
                      return 'La presión sistólica es requerida.';
                    }

                    return null;
                  },
                  onSaved: (bpDiastolic) {
                    _ppgStoreRequest.bpDiastolic = int.parse(bpDiastolic!);
                  }),
              TextFormField(
                  initialValue: doubleInitialValue,
                  decoration:
                      const InputDecoration(labelText: 'Glucosa medida'),
                  textInputAction: TextInputAction.next,
                  focusNode: _trueGlucoFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_subjectAgeFocusNode);
                  },
                  validator: (trueGluco) {
                    if (trueGluco!.isEmpty) {
                      return 'El valor medido de glucosa es requerido.';
                    }

                    return null;
                  },
                  onSaved: (trueGluco) {
                    _ppgStoreRequest.trueGluco = double.parse(trueGluco!);
                  }),
              TextFormField(
                  initialValue: doubleInitialValue,
                  decoration: const InputDecoration(labelText: 'Edad'),
                  textInputAction: TextInputAction.next,
                  focusNode: _subjectAgeFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context)
                        .requestFocus(_subjectHeightFocusNode);
                  },
                  validator: (subjectAge) {
                    if (subjectAge!.isEmpty) {
                      return 'La edad es requerida.';
                    }

                    return null;
                  },
                  onSaved: (subjectAge) {
                    _ppgStoreRequest.subjectAge = int.parse(subjectAge!);
                  }),
              TextFormField(
                  initialValue: doubleInitialValue,
                  decoration: const InputDecoration(labelText: 'Estatura (cm)'),
                  textInputAction: TextInputAction.next,
                  focusNode: _subjectHeightFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context)
                        .requestFocus(_subjectWeightFocusNode);
                  },
                  validator: (subjectHeight) {
                    if (subjectHeight!.isEmpty) {
                      return 'La estatura es requerida.';
                    }

                    return null;
                  },
                  onSaved: (subjectHeight) {
                    _ppgStoreRequest.subjectHeight =
                        double.parse(subjectHeight!);
                  }),
              TextFormField(
                  initialValue: doubleInitialValue,
                  decoration: const InputDecoration(labelText: 'Peso (kg)'),
                  textInputAction: TextInputAction.next,
                  focusNode: _subjectWeightFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_subjectSexFocusNode);
                  },
                  validator: (subjectWeight) {
                    if (subjectWeight!.isEmpty) {
                      return 'El peso es requerido.';
                    }

                    return null;
                  },
                  onSaved: (subjectWeight) {
                    _ppgStoreRequest.subjectWeight =
                        double.parse(subjectWeight!);
                  }),
              DropdownButtonFormField<String>(
                  value: dropdownInitialValue,
                  decoration: const InputDecoration(labelText: 'Sexo'),
                  focusNode: _subjectSexFocusNode,
                  items: const [
                    DropdownMenuItem(
                      value: 'male',
                      child: Text('masculino'),
                    ),
                    DropdownMenuItem(
                      value: 'female',
                      child: Text('femenino'),
                    ),
                  ],
                  onChanged: (subjectSex) {
                    FocusScope.of(context)
                        .requestFocus(_subjectIsDiabeticFocusNode);
                  },
                  validator: (subjectSex) {
                    if (subjectSex == null || subjectSex.isEmpty) {
                      return 'Este campo es requerido.';
                    }

                    return null;
                  },
                  onSaved: (subjectSex) {
                    setState(() {
                      _ppgStoreRequest.subjectSex = subjectSex!;
                    });
                  }),
              DropdownButtonFormField<String>(
                  value: dropdownInitialValue,
                  decoration:
                      const InputDecoration(labelText: '¿Tiene diabetes?'),
                  focusNode: _subjectIsDiabeticFocusNode,
                  items: const [
                    DropdownMenuItem(
                      value: 'true',
                      child: Text('Sí'),
                    ),
                    DropdownMenuItem(
                      value: 'false',
                      child: Text('No'),
                    ),
                  ],
                  onChanged: (subjectIsDiabetic) {
                    FocusScope.of(context).requestFocus(_subjectEmailFocusNode);
                  },
                  validator: (subjectIsDiabetic) {
                    return null;
                  },
                  onSaved: (subjectIsDiabetic) {
                    setState(() {
                      _ppgStoreRequest.subjectIsDiabetic =
                          subjectIsDiabetic == 'true';
                    });
                  }),
              TextFormField(
                  initialValue: _ppgStoreRequest.subjectEmail.toString(),
                  decoration: const InputDecoration(
                      labelText: '(opcional) Correo electrónico'),
                  textInputAction: TextInputAction.done,
                  focusNode: _subjectEmailFocusNode,
                  onFieldSubmitted: (_) {
                    storePpg();
                  },
                  validator: (subjectEmail) {
                    return null;
                  },
                  onSaved: (subjectEmail) {
                    _ppgStoreRequest.subjectEmail = subjectEmail!;
                  }),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Regresar',
                          style: Theme.of(context).textTheme.button)),
                  ElevatedButton(
                      onPressed: _processing ? null : storePpg,
                      child: Text(_processing ? 'Enviando datos...' : 'Enviar datos',
                          style: Theme.of(context).textTheme.button)),
                ],
              )
            ]),
          ),
        )));
  }
}

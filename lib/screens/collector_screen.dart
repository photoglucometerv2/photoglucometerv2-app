import 'dart:async';
import 'dart:convert';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:gluco_mobile_app/screens/create_ppg_screen.dart';

import '../models/point.dart';
import '../widgets/chart.dart';
import '../widgets/app_drawer.dart';

enum ConnectionStatus { notConnected, connecting, connected, failed }

enum CollectionStatus { stopped, notCollecting, collecting }

class CollectorScreen extends StatefulWidget {
  static const routeName = "/collector";
  final BluetoothDevice? device;

  CollectorScreen({required this.device});

  @override
  _CollectorScreenState createState() {
    return _CollectorScreenState();
  }
}

class _CollectorScreenState extends State<CollectorScreen> {
  final List<Point> _ppg = [];
  final List<Point> _ppgAlt = [];
  final List<Point> _ecg = [];
  var _connectionStatus = ConnectionStatus.notConnected;
  var _collectionStatus = CollectionStatus.notCollecting;

  String _getId(String uuid) {
    return uuid.toString().split('-')[0].substring(4);
  }

  Future<void> _displayErrorDialog() async {
    setState(() {
      _connectionStatus = ConnectionStatus.failed;
      clearTimeSeries();
    });

    await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: const Text("Error de conexión"),
              content:
                  const Text("Sucedió un error con la conexión al dispositivo"),
              actions: [
                TextButton(
                  child: const Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ]);
        });
  }

  void trimTimeSeries(List<Point> ppg) {
    if (ppg.length > 3000) {
      ppg.removeRange(3001, ppg.length);
    }
  }

  void clearTimeSeries() {
    setState(() {
      _ppg.clear();
      _ppgAlt.clear();
      _ecg.clear();
    });
  }

  void stopCollecting() {
    clearTimeSeries();
    setState(() {
      _collectionStatus = CollectionStatus.notCollecting;
    });
  }

  void startCollecting() {
    clearTimeSeries();
    setState(() {
      _collectionStatus = CollectionStatus.collecting;
    });
  }

  Future<void> _displaySuccessDialog() async {
    trimTimeSeries(_ppg);
    trimTimeSeries(_ppgAlt);
    trimTimeSeries(_ecg);

    await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: const Text("Señal capturada"),
              content: Text(
                  "Se capturaron ${_ppg.length} puntos. Desea enviar esta señal al servidor?"),
              actions: [
                TextButton(
                  child: const Text('Sí, envíar'),
                  onPressed: () async {
                    await Navigator.of(context)
                        .pushNamed(CreatePpgScreen.routeName, arguments: {
                      "ppg": _ppg.map((point) => point.value).join(","),
                      "ppgAlt": _ppgAlt.map((point) => point.value).join(","),
                      "ecg": _ecg.map((point) => point.value).join(",")
                    });

                    stopCollecting();
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Cancelar'),
                  onPressed: () {
                    stopCollecting();
                    Navigator.of(context).pop();
                  },
                )
              ]);
        });
  }

  Future<void> _readFromDevice() async {
    if (widget.device != null) {
      try {
        setState(() {
          _connectionStatus = ConnectionStatus.connecting;
        });

        await widget.device?.disconnect();
        await widget.device?.connect();

        setState(() {
          _connectionStatus = ConnectionStatus.connected;
        });

        final services = await widget.device?.discoverServices();
        final service = services?.firstWhere((service) {
          return _getId(service.uuid.toString()) == 'ffe0';
        });

        final characteristics = service?.characteristics;
        final characteristic = characteristics?.firstWhere((characteristic) {
          return _getId(characteristic.uuid.toString()) == 'ffe1';
        });

        int buffer = 0;
        await characteristic?. setNotifyValue(true);
        characteristic?.value.listen((value) {
          String decodedValue = ascii.decode(value);

          decodedValue.split('').forEach((character) {
            var digit = int.tryParse(character);

            if (digit != null) {
              buffer = buffer * 10 + digit;
            } else if (character == ',' ||
                character == ';' ||
                character == '\r') {
              setState(() {
                final ppg = character == ','
                    ? _ppg
                    : character == ';'
                        ? _ppgAlt
                        : _ecg;

                if (_collectionStatus != CollectionStatus.stopped) {
                  ppg.add(Point(time: DateTime.now(), value: buffer));
                }

                if (_collectionStatus == CollectionStatus.notCollecting &&
                    ppg.length > 300) {
                  ppg.removeAt(0);
                }

                if (_collectionStatus == CollectionStatus.collecting &&
                    _ecg.length == 3000) {
                  _collectionStatus = CollectionStatus.stopped;
                  _displaySuccessDialog();
                }
              });

              buffer = 0;
            }
          });
        }, onDone: () async {
          await _displayErrorDialog();
        }, onError: (error) async {
          await _displayErrorDialog();
        });
      } catch (error) {
        await _displayErrorDialog();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Recolectar datos')),
      drawer: AppDrawer(),
      body: SingleChildScrollView(
          child: Column(children: [
        const SizedBox(height: 20),
        const Center(child: Text('PPG dedo')),
        Container(
            height: 150,
            margin: const EdgeInsets.all(12),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18), color: Colors.black),
            child: _connectionStatus == ConnectionStatus.connected
                ? Chart(ppg: _ppg)
                : null),
        const SizedBox(height: 20),
        const Center(child: Text('PPG oreja')),
        Container(
            height: 150,
            margin: const EdgeInsets.all(12),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18), color: Colors.black),
            child: _connectionStatus == ConnectionStatus.connected
                ? Chart(ppg: _ppgAlt)
                : null),
        const SizedBox(height: 20),
        const Center(child: Text('ECG')),
        Container(
            height: 150,
            margin: const EdgeInsets.all(12),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18), color: Colors.black),
            child: _connectionStatus == ConnectionStatus.connected
                ? Chart(ppg: _ecg)
                : null),
        const SizedBox(height: 20),
        widget.device == null
            ? const Center(child: Text('No se ha seleccionado un dispositivo'))
            : _connectionStatus == ConnectionStatus.notConnected
                ? Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                        const Text(
                          'Dispositivo listo',
                        ),
                        ElevatedButton(
                            child: const Text('Conectar'),
                            onPressed: () {
                              _readFromDevice();
                            })
                      ]))
                : _connectionStatus == ConnectionStatus.connected
                    ? Center(
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                            Text(_collectionStatus == CollectionStatus.collecting ? '${_ecg.length} muestras recolectadas' : 'Dispositivo conectado'),
                            ElevatedButton(
                                child: Text(_connectionStatus ==
                                            ConnectionStatus.connected &&
                                        _collectionStatus ==
                                            CollectionStatus.collecting
                                    ? 'Captura en progreso...'
                                    : 'Comenzar captura'),
                                onPressed: _connectionStatus !=
                                            ConnectionStatus.connected ||
                                        _collectionStatus ==
                                            CollectionStatus.collecting
                                    ? null
                                    : () {
                                        startCollecting();
                                      })
                          ]))
                    : _connectionStatus == ConnectionStatus.connecting
                        ? const Center(
                            child: Text('Conectando con dispositivo'))
                        : Center(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                const Text(
                                  'No se pudo conectar con el dispositivo',
                                ),
                                ElevatedButton(
                                    child: const Text('Reintentar'),
                                    onPressed: () {
                                      _readFromDevice();
                                    })
                              ])),
      ])),
    );
  }
}

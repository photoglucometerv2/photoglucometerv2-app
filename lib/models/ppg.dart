class PpgStoreRequest {
  String ppg;
  String ppgAlt;
  String ecg;
  int bpSystolic;
  int bpDiastolic;
  num trueGluco;
  int subjectAge;
  num subjectHeight;
  num subjectWeight;
  String subjectSex;
  bool subjectIsDiabetic;
  String? subjectEmail;

  PpgStoreRequest(
      {required this.ppg,
      required this.ppgAlt,
      required this.ecg,
      required this.bpSystolic,
      required this.bpDiastolic,
      required this.trueGluco,
      required this.subjectAge,
      required this.subjectHeight,
      required this.subjectWeight,
      required this.subjectSex,
      required this.subjectIsDiabetic,
      this.subjectEmail});
}

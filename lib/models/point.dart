class Point{
    final DateTime time;
    final int value;

    Point({
        required this.time,
        required this.value
    });
}
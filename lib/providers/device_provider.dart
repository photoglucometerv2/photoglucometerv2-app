import 'package:flutter/cupertino.dart';
import 'package:flutter_blue/flutter_blue.dart';

import '../models/app_exception.dart';

class DeviceProvider extends ChangeNotifier{
    final compatibleDevices = ['6C:79:B8:B4:81:D4', 'B4:52:A9:04:E0:95'];

    BluetoothDevice? _device;

    BluetoothDevice? get device{
        return _device;
    }

    void setDevice(BluetoothDevice device){
        if(!compatibleDevices.contains(device.id.toString())){
            throw AppException('Dispositivo no compatible');
        }

        _device = device;
        notifyListeners();
    }
}
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

//Models
import '../models/ppg.dart';
import '../models/http_exception.dart';

class PpgProvider with ChangeNotifier {
  Future<void> store(PpgStoreRequest ppgStoreRequest) async {
    final url = 'http://192.168.86.34:3000/ppgs';

    try {
      final response = await http.post(
        Uri.parse(url),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
        body: json.encode({
          'ppg': ppgStoreRequest.ppg,
          'ppg_alt': ppgStoreRequest.ppgAlt,
          'ecg': ppgStoreRequest.ecg,
          'bp_systolic': ppgStoreRequest.bpSystolic,
          'bp_diastolic': ppgStoreRequest.bpDiastolic,
          'true_gluco': ppgStoreRequest.trueGluco,
          'subject_age': ppgStoreRequest.subjectAge,
          'subject_height': ppgStoreRequest.subjectHeight,
          'subject_weight': ppgStoreRequest.subjectWeight,
          'subject_sex': ppgStoreRequest.subjectSex,
          'subject_isdiabetic': ppgStoreRequest.subjectIsDiabetic,
          'subject_email': ppgStoreRequest.subjectEmail
        }),
      );

      if (response.statusCode >= 400) {
        final error = json.decode(response.body) as Map<String, dynamic>;
        throw HttpException(error['message']);
      }
    } catch (error) {
      print(error);
      throw HttpException(error.toString());
    }
  }
}
